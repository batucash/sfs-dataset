#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 21:18:07 2019

@author: batuhan
"""

import numpy as np
from time import time
from numpy import vectorize
import pandas as pd
from sklearn import svm
#import matplotlib.pyplot as plt
from skimage.feature import hog
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import train_test_split 
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler,binarize
import os
from skimage.transform import resize
from skimage import data,img_as_ubyte,img_as_float
from skimage.color import rgb2gray
from skimage.filters import prewitt,sobel,scharr,gaussian
from skimage.morphology import disk,square
from skimage.filters import median
from sklearn.base import ClusterMixin
from scipy.cluster.vq import kmeans2
import seaborn as sb
from scipy.cluster.hierarchy import fclusterdata as fc
#%% TIME CALCULATION FOR KMEANS
b = pd.read_excel('semaphore_research_related.xlsx')
sb.set_style("whitegrid")
ax = sb.swarmplot(x="Dimensionality", y="Accuracy",hue="Feature",
                 data=b,palette="muted",size=12,split=False)
#%%
times =0
ctr = 0
for i in range(100):
    t=time()
    
    img_gray2=img_as_ubyte(rgb2gray(img2))#.astype('uint8')
    img_diff = img_as_ubyte((img_as_float(img2[:,:,0])-img_as_float(img_gray2)))
    med = median(img_diff, disk(2))
    binary=binarize(med,threshold=64)
    locs=np.asarray(np.where(binary))
    #neigh,_=kmeans2(locs.transpose().astype('float'),k=[[140,20],[140,120]],minit='matrix')#NearestNeighbors(n_neighbors=2,metric='euclidean')
    neigh=fc(locs.transpose().astype('float'),2,'maxclust','euclidean')#NearestNeighbors(n_neighbors=2,metric='euclidean')
    a = np.where(neigh==1)
    b1 = np.mean(locs.transpose()[a],axis=0)
    a = np.where(neigh==2)
    b2 = np.mean(locs.transpose()[a],axis=0)
    arr=np.vstack((b1,b2))
    arr[:,arr[0,:].argsort()]
    
    times+=(time()-t)
    ctr+=1
print(times/ctr)
#%% LOAD THE RAW DATASET
     
hog_set=[]
#os.chdir('/Users/batuhan/Desktop/Research/IDEAS/5.\ Semaphore')
dataset_dir = '/Users/batuhan/Desktop/Research/IDEAS/5. Semaphore/dataset'
classes = os.listdir(dataset_dir)
labels=[]
locs_mat=[]
raw_gray=[]
label=0
total_time=0
ctr=0;
for f in classes:
   if not f.startswith('.'):
       class_file=os.path.join(dataset_dir,f)
       samples=os.listdir(class_file)
       for sample in samples:         
           sample_name=os.path.join(class_file,sample)
           img = data.load(sample_name)
           img2 = img[::4,::4,:]
           img_gray=rgb2gray(img)
           img_gray2=img_as_ubyte(rgb2gray(img2))
           img_diff = img_as_ubyte((img_as_float(img2[:,:,0])-img_as_float(img_gray2)))
           med = median(img_diff, disk(2))
           binary=binarize(med,threshold=64)
           locs=np.asarray(np.where(binary))
           #neigh,_=kmeans2(locs.transpose().astype('float'),k=[[140,20],[140,120]],minit='matrix')
           neigh=fc(locs.transpose().astype('float'),2,'maxclust','euclidean')#NearestNeighbors(n_neighbors=2,metric='euclidean')
           a = np.where(neigh==1)
           b1 = np.mean(locs.transpose()[a],axis=0)
           a = np.where(neigh==2)
           b2 = np.mean(locs.transpose()[a],axis=0)
           arr=np.vstack((b1,b2))
           arr[:,arr[0,:].argsort()]
           img_vec = img_gray.reshape((img_gray.shape[0]*img_gray.shape[1],))
           locs_vec=arr.reshape((arr.shape[0]*arr.shape[1],))
           img_gray2 = resize(img_gray2,(256,128)) 
           t=time()
           fd = hog(binary)
           total_time+=(time()-t)
           hog_set.append(fd)
           raw_gray.append(img_vec)
           locs_mat.append(locs_vec)
           labels.append(f)
raw_gray=np.asarray(raw_gray)
locs_mat=np.asarray(locs_mat)
hog_set=np.asarray(hog_set)
print(total_time)
#%%   CLASSIFICATION WITH PCA ON GRAY
som=0
ctr=0    
X= np.arange(520)
train_time=0
feat_time=0       
for i in range(10):        
    X_train, X_test, y_train, y_test = train_test_split(X, labels,
                                        test_size=0.5, random_state=i)
    
    t=time()
    pca=PCA(n_components=16)
    X_train=pca.fit_transform(hog_set[X_train])
    train_time+=(time()-t)
    t2 = time()
    X_test=pca.transform(hog_set[X_test])
    feat_time+=(time()-t2)

    clf=svm.SVC(kernel='linear')
    t3=time()
    clf.fit(X_train,y_train)
    train_time+=(time()-t3)
    y=clf.predict(X_test)
    print('accuracy: ',accuracy_score(y_test,y))    
    som+=accuracy_score(y_test,y)
    ctr+=1
print('averge accuracy: ', som/ctr)
print('feat time: ', feat_time/ctr)
print('train time: ', train_time/ctr)
        
#%%   CLASSIFICATION WITH CENTROIDS
som=0
ctr=0    
train_time=0
X= np.arange(520)      
for i in range(10):        
    X_train, X_test, y_train, y_test = train_test_split(locs_mat, labels,
                                        test_size=0.5, random_state=i)
    

    #pca=PCA(n_components=4)
    #print('training pca')
    #X_train=pca.fit_transform(X_train)
    #print('transforming test')
    #t = time()
    #X_test=pca.transform(X_test)
    #print(time()-t)
    #clf=svm.SVC()
    #print('training svm')
    #clf.fit(X_train,y_train)
    
    clf=svm.SVC(kernel='poly')
    t=time()
    clf.fit(X_train,y_train)
    train_time+=(time()-t)
    y=clf.predict(X_test)
    #print(accuracy_score(y_test,y))
    som+=accuracy_score(y_test,y)
    ctr+=1
print('averge accuracy: ', som/ctr)
print('train time: ', train_time/ctr)  
#%%        

som=0;
ctr=0;
hg=np.zeros((41310,520))
#for i in range(520):
#    bu = np.reshape(train_gray[i,:].transpose(),(1152/4,568/4),order='F')
#    hg[:,i] = hog(bu)






norm = 'L1-sqrt'



sc = StandardScaler()
acc = 0;
ort_a=np.zeros((10,))


final_score = acc/10
print('Ortalama: ',final_score)
print('Sigma: ', np.std(ort_a))

cm = confusion_matrix(y_test, out)
cm1 = np.zeros_like(cm)
map1 = (0,1,3,5,8,2,4,6,7)

for i in range(9):
    j = map1[i]
    cm1[i,:] = cm[j,:]

cm2 = np.copy(cm1)

for i in range(9):
    j = map1[i]
    cm2[:,i] = cm1[:,j]

#for i in range(9):
#    cm2[i] = cm2[i]/np.sum(cm2[i])
cmap=plt.cm.Reds
  
cm2 = cm2.astype('float') / cm2.sum(axis=1)[:, np.newaxis]

fig, ax = plt.subplots()
im = ax.imshow(cm2, interpolation='nearest', cmap=cmap)
ax.figure.colorbar(im, ax=ax)
ax.set(xticks=np.arange(cm.shape[1]),
       yticks=np.arange(cm.shape[0]),
       xticklabels=('Ada','Burak','Gabya','Meko','YTKB','Doğan','Kılıç','Rüzgar','Yıldız'), 
       yticklabels=('Ada','Burak','Gabya','Meko','YTKB','Doğan','Kılıç','Rüzgar','Yıldız'))

plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=True,
                          title=None,
                          cmap=plt.cm.Reds):
    
    cm = confusion_matrix(y_test, out)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           xticklabels=classes, 
           yticklabels=classes,
           title=title)

    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

#plot_confusion_matrix(y_test, out, classes=classes, normalize=True)

plt.imshow(cm2)
